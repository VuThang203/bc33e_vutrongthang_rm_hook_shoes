import logo from "./logo.svg";
import "./App.css";
import Ex_Shoes_Hook from "./Ex_Shoes_Hook/Ex_Shoes_Hook";

function App() {
  return (
    <div className="App">
      <Ex_Shoes_Hook />
    </div>
  );
}

export default App;
