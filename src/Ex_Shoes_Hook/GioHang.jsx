import React from "react";

export default function GioHang({ gioHang, handleChangeQuantity }) {
  let renderTbody = () => {
    return gioHang.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.quantity}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-5">{item.quantity}</span>
            <button
              onClick={() => {
                handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Price</th>
            <th>Hình ảnh</th>
            <th>Số lượng</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
