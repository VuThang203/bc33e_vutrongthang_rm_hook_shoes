import React from "react";

export default function ItemShoes({ dataShoes, handleAddToCart }) {
  return (
    <div className="card mb-5">
      <img className="card-img-top" src={dataShoes.image} alt="" />
      <div className="card-body">
        <h4 className="card-title">{dataShoes.name}</h4>
        <p className="card-text">{dataShoes.price} $</p>
        <button
          onClick={() => {
            handleAddToCart(dataShoes);
          }}
          className="btn btn-dark"
        >
          Add To Card
        </button>
      </div>
    </div>
  );
}
