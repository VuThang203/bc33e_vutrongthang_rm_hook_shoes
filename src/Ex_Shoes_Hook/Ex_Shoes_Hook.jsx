import React, { useState, useEffect } from "react";
import GioHang from "./GioHang";
import { data_shoes } from "./data_shoes";
import ItemShoes from "./ItemShoes";

export default function Ex_Shoes_Hook() {
  const [shoes, setShoes] = useState({
    listShoes: data_shoes,
    cart: [],
  });

  let renderShoesList = () => {
    return shoes.listShoes.map((item, index) => {
      return (
        <div className="col-4" key={index}>
          <ItemShoes handleAddToCart={handleAddToCart} dataShoes={item} />
        </div>
      );
    });
  };

  let handleAddToCart = (itemShoes) => {
    let index = shoes.cart.findIndex((item) => {
      return item.id == itemShoes.id;
    });
    let cloneCart = [...shoes.cart];
    if (index == -1) {
      let newSp = { ...itemShoes, quantity: 1 };
      cloneCart.push(newSp);
    } else {
      cloneCart[index].quantity++;
    }
    setShoes({ ...shoes, cart: cloneCart });
  };

  let handleChangeQuantity = (idShoes, value) => {
    let index = shoes.cart.findIndex((item) => {
      return item.id == idShoes;
    });
    if (index == -1) return;
    let cloneCart = [...shoes.cart];
    cloneCart[index].quantity = cloneCart[index].quantity + value;
    cloneCart[index].quantity == 0 && cloneCart.splice(index, 1);
    setShoes({ ...shoes, cart: cloneCart });
  };

  return (
    <div className="container">
      <GioHang
        handleChangeQuantity={handleChangeQuantity}
        gioHang={shoes.cart}
      />
      <div className="row">{renderShoesList()}</div>
    </div>
  );
}
